package ro.tuc.ds2020.dtos;

import ro.tuc.ds2020.entities.Answer;
import ro.tuc.ds2020.entities.Question;

import java.util.List;

public class UserDTO {

    private long id;
    private String username;
    private String password;
    private boolean isAdmin;

    public UserDTO() {
    }

    public UserDTO(long id, String username, String password, boolean isAdmin) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.isAdmin = isAdmin;
    }
    
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }
}
