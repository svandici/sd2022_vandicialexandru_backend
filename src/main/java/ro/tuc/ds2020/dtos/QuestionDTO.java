package ro.tuc.ds2020.dtos;

import ro.tuc.ds2020.entities.Answer;
import ro.tuc.ds2020.entities.Tag;
import ro.tuc.ds2020.entities.User;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class QuestionDTO {

    private long id;
    private UserDTO author;
    private String title;
    private String text;
    private LocalDateTime date;
    private int upvotes;
    private List<TagDTO> tagsList = new ArrayList<>();

    public QuestionDTO() {
    }

    public QuestionDTO(long id, UserDTO author, String title, String text, LocalDateTime date, int upvotes, List<TagDTO> tagsList) {
        this.id = id;
        this.author = author;
        this.title = title;
        this.text = text;
        this.date = date;
        this.upvotes = upvotes;
        this.tagsList = tagsList;
    }

    public List<TagDTO> getTagsList() {
        return tagsList;
    }

    public void setTagsList(List<TagDTO> tagsList) {
        this.tagsList = tagsList;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public UserDTO getAuthor() {
        return author;
    }

    public void setAuthor(UserDTO author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public int getUpvotes() {
        return upvotes;
    }

    public void setUpvotes(int upvotes) {
        this.upvotes = upvotes;
    }
}
