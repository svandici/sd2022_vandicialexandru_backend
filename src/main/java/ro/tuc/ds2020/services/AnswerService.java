package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.builders.AnswerBuilder;
import ro.tuc.ds2020.builders.QuestionBuilder;
import ro.tuc.ds2020.builders.UserBuilder;
import ro.tuc.ds2020.dtos.AnswerDTO;
import ro.tuc.ds2020.entities.Answer;
import ro.tuc.ds2020.repositories.IAnswerRepository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class AnswerService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);
    private final IAnswerRepository answerRepository;


    public AnswerService(IAnswerRepository answerRepository) {
        this.answerRepository = answerRepository;
    }

    public List<AnswerDTO> getAllAnswers() {
        List<Answer> answerList = answerRepository.findAll();

        return answerList.stream()
                .map(AnswerBuilder::toDTO)
                .collect(Collectors.toList());
    }

    public AnswerDTO getAnswerById(long id) {
        Optional<Answer> answerOptional = answerRepository.findById(id);

        if (!answerOptional.isPresent()) {
            LOGGER.error("Answer with id {} was not found in db", id);
            throw new ResourceNotFoundException(Answer.class.getSimpleName() + " with id: " + id);
        }

        return AnswerBuilder.toDTO(answerOptional.get());
    }

    public void deleteAnswer(long id) {
        answerRepository.deleteById(id);
    }

    public AnswerDTO saveAnswer(AnswerDTO answerDTO) {
        Answer answer = AnswerBuilder.toEntity(answerDTO);
        return AnswerBuilder.toDTO(answerRepository.save(answer));
    }

    public AnswerDTO updateAnswer(long id, AnswerDTO answerDTO) {
        Optional<Answer> answerOptional = answerRepository.findById(id);

        if (!answerOptional.isPresent()) {
            LOGGER.error("Answer with id {} was not found in db", id);
            throw new ResourceNotFoundException(Answer.class.getSimpleName() + " with id: " + id);
        }

        Answer answer = answerOptional.get();
        answer.setText(answerDTO.getText());
        answer.setUpvotes(answerDTO.getUpvotes());
        answer.setAuthor(UserBuilder.toEntity(answerDTO.getAuthor()));
        answer.setQuestion(QuestionBuilder.toEntity(answerDTO.getQuestion()));

        return AnswerBuilder.toDTO(answerRepository.save(answer));
    }

    public List<AnswerDTO> getAnswersByUser(long userId)
    {
        List<Answer> answersByUser = answerRepository.findAll().stream().filter(a -> a.getAuthor().getId() == userId).collect(Collectors.toList());

        return answersByUser.stream().map(AnswerBuilder::toDTO).collect(Collectors.toList());
    }

    public List<AnswerDTO> getAnswersByQuestion(long questionId) {
        List<Answer> answersByQuestion = answerRepository.findAll().stream().filter(a -> a.getQuestion().getId() == questionId).collect(Collectors.toList());

        return answersByQuestion.stream().map(AnswerBuilder::toDTO).collect(Collectors.toList());
    }
}
