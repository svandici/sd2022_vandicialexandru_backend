package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.builders.TagBuilder;
import ro.tuc.ds2020.dtos.TagDTO;
import ro.tuc.ds2020.entities.Tag;
import ro.tuc.ds2020.repositories.ITagRepository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class TagService {

    private static final Logger LOGGER = LoggerFactory.getLogger(TagService.class);
    private final ITagRepository tagRepository;

    public TagService(ITagRepository tagRepository) {
        this.tagRepository = tagRepository;
    }

    public List<TagDTO> getAllTags() {
        List<Tag> tagList = tagRepository.findAll();

        return tagList.stream()
                .map(TagBuilder::toDTO)
                .collect(Collectors.toList());
    }

    public TagDTO getTagById(long id) {
        Optional<Tag> tagOptional = tagRepository.findById(id);

        if (!tagOptional.isPresent()) {
            LOGGER.error("Tag with id {} was not found in db", id);
            throw new ResourceNotFoundException(Tag.class.getSimpleName() + " with id: " + id);
        }

        return TagBuilder.toDTO(tagOptional.get());
    }

    public void deleteTag(long id) {
        tagRepository.deleteById(id);
    }

    public TagDTO saveTag(TagDTO tagdTO) {
        Tag tag = TagBuilder.toEntity(tagdTO);
        return TagBuilder.toDTO(tagRepository.save(tag));
    }

    public TagDTO updateTag(long id, TagDTO tagDTO) {

        Optional<Tag> tagOptional = tagRepository.findById(id);
        if (!tagOptional.isPresent()) {
            LOGGER.error("Tag with id {} was not found in db", id);
            throw new ResourceNotFoundException(Tag.class.getSimpleName() + " with id: " + id);
        }

        Tag tag = tagOptional.get();

        tag.setName(tagDTO.getName());

        return TagBuilder.toDTO(tagRepository.save(tag));


    }
}
