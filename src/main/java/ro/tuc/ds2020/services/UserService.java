package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.builders.UserBuilder;
import ro.tuc.ds2020.dtos.UserDTO;
import ro.tuc.ds2020.entities.User;
import ro.tuc.ds2020.repositories.IUserRepository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);
    private final IUserRepository userRepository;

    public UserService(IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<UserDTO> getAllUsers() {
        List<User> userList = userRepository.findAll();

        return userList.stream()
                .map(UserBuilder::toDTO)
                .collect(Collectors.toList());
    }

    public UserDTO getUserById(long id) {
        Optional<User> userOptional = userRepository.findById(id);

        if (!userOptional.isPresent()) {
            LOGGER.error("User with id {} was not found in db", id);
            throw new ResourceNotFoundException(User.class.getSimpleName() + " with id: " + id);
        }

        return UserBuilder.toDTO(userOptional.get());
    }

    public void deleteUser(long id) {
        userRepository.deleteById(id);
    }

    public UserDTO saveUser(UserDTO userdto) {
        User user = UserBuilder.toEntity(userdto);
        return UserBuilder.toDTO(userRepository.save(user));
    }

    public UserDTO updateUser(UserDTO userDTO) {
        Optional<User> userOptional = userRepository.findById(userDTO.getId());

        if (!userOptional.isPresent()) {
            LOGGER.error("User with id {} was not found in db", userDTO.getId());
            throw new ResourceNotFoundException(User.class.getSimpleName() + " with id: " + userDTO.getId());
        }

        User user = userOptional.get();

        user.setPassword(userDTO.getPassword());
        user.setUsername(userDTO.getUsername());
        user.setAdmin(userDTO.isAdmin());

        return UserBuilder.toDTO(userRepository.save(user));
    }


}
