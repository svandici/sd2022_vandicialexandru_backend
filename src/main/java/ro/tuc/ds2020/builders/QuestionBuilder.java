package ro.tuc.ds2020.builders;

import ro.tuc.ds2020.dtos.QuestionDTO;
import ro.tuc.ds2020.entities.Question;

import java.util.stream.Collectors;

public class QuestionBuilder {

    public static QuestionDTO toDTO(Question question) {
        return new QuestionDTO(question.getId(), UserBuilder.toDTO(question.getAuthor()), question.getTitle(), question.getText(), question.getDate(), question.getUpvotes(), question.getTagsList().stream().map(TagBuilder::toDTO).collect(Collectors.toList()));
    }

    public static Question toEntity(QuestionDTO questionDTO) {
        return new Question(questionDTO.getId(), UserBuilder.toEntity(questionDTO.getAuthor()), questionDTO.getTitle(), questionDTO.getText(), questionDTO.getDate(), questionDTO.getUpvotes(), questionDTO.getTagsList().stream().map(TagBuilder::toEntity).collect(Collectors.toList()));
    }
}
