package ro.tuc.ds2020.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.QuestionDTO;
import ro.tuc.ds2020.services.QuestionService;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/question")
public class QuestionController {

    private final QuestionService questionService;

    @Autowired
    public QuestionController(QuestionService questionService) {
        this.questionService = questionService;
    }

    @GetMapping
    public ResponseEntity<List<QuestionDTO>> getQuestions() {
        return new ResponseEntity<>(questionService.getAllQuestions(), HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<QuestionDTO> getQuestion(@PathVariable("id") long id) {
        return new ResponseEntity<>(questionService.getQuestionById(id), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity deleteQuestion(@PathVariable("id") long id) {
        questionService.deleteQuestion(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<QuestionDTO> saveQuestion(@Valid @RequestBody QuestionDTO question) {
        return new ResponseEntity<>(questionService.saveQuestion(question), HttpStatus.CREATED);
    }


    @PutMapping()
    public ResponseEntity updateQuestion(@Valid @RequestBody QuestionDTO question) {
        return new ResponseEntity<>(questionService.updateQuestion(question.getId(), question), HttpStatus.OK);
    }

    @GetMapping(value = "/byUser/{id}")
    public ResponseEntity<List<QuestionDTO>> getQuestionsByUser(@PathVariable("id") long userId) {
        return new ResponseEntity<>(questionService.getQuestionsByUser(userId), HttpStatus.OK);
    }
}
