package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.AnswerDTO;
import ro.tuc.ds2020.services.AnswerService;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/answer")
public class AnswerController {
    private final AnswerService answerService;

    @Autowired
    public AnswerController(AnswerService answerService) {
        this.answerService = answerService;
    }

    @GetMapping
    public ResponseEntity<List<AnswerDTO>> getAnswers() {
        return new ResponseEntity<>(answerService.getAllAnswers(), HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<AnswerDTO> getAnswer(@PathVariable("id") long id) {
        return new ResponseEntity<>(answerService.getAnswerById(id), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity deleteAnswer(@PathVariable("id") long id) {
        answerService.deleteAnswer(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<AnswerDTO> saveAnswer(@Valid @RequestBody AnswerDTO answer) {
        return new ResponseEntity<>(answerService.saveAnswer(answer), HttpStatus.CREATED);
    }


    @PutMapping()
    public ResponseEntity updateAnswer(@Valid @RequestBody AnswerDTO answer) {
        return new ResponseEntity<>(answerService.updateAnswer(answer.getId(), answer), HttpStatus.OK);
    }

    @GetMapping(value = "/byUser/{id}")
    public ResponseEntity<List<AnswerDTO>> getAnswersByUser(@PathVariable("id") long userId) {
        return new ResponseEntity<>(answerService.getAnswersByUser(userId), HttpStatus.OK);
    }

    @GetMapping(value = "/byQuestion/{id}")
    public ResponseEntity<List<AnswerDTO>> getAnswersByQuestion(@PathVariable("id") long questionId) {
        return new ResponseEntity<>(answerService.getAnswersByQuestion(questionId), HttpStatus.OK);
    }
}
