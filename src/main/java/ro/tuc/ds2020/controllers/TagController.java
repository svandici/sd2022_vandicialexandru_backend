package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.TagDTO;
import ro.tuc.ds2020.services.TagService;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/tag")
public class TagController {
    private final TagService tagService;

    @Autowired
    public TagController(TagService tagService) {
        this.tagService = tagService;
    }

    @GetMapping
    public ResponseEntity<List<TagDTO>> getTags() {
        return new ResponseEntity<>(tagService.getAllTags(), HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<TagDTO> getTag(@PathVariable("id") long id) {
        return new ResponseEntity<>(tagService.getTagById(id), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity deleteTag(@PathVariable("id") long id) {
        tagService.deleteTag(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<TagDTO> saveTag(@Valid @RequestBody TagDTO tagDTO) {
        return new ResponseEntity<>(tagService.saveTag(tagDTO), HttpStatus.CREATED);
    }


    @PutMapping()
    public ResponseEntity updateTag(@Valid @RequestBody TagDTO tagDTO) {
        return new ResponseEntity<>(tagService.updateTag(tagDTO.getId(), tagDTO), HttpStatus.OK);
    }
}
