package ro.tuc.ds2020.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ro.tuc.ds2020.entities.User;

public interface IUserRepository extends JpaRepository<User, Long> {
}
